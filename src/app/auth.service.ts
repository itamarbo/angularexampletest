import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { User } from './interfaces/user';
import { Router, ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  
  user: Observable<User | null>;
  text:boolean = false;

  constructor(public afAuth:AngularFireAuth, private router:Router, private route:ActivatedRoute ) {
    this.user = this.afAuth.authState;
   }

   signup(email:string, password:string){
    this.afAuth
    .auth.createUserWithEmailAndPassword(email,password).then
    (res => { console.log('Succesful Signup', res);
    this.router.navigate(['/successlogin']);
  }
   )
    .catch(function(error) {
     // Handle Errors here.
     // var errorCode = error.code;
     var errorCode = error.code;
     var errorMessage = error.message;
     alert(errorMessage);
     console.log(error);
     console.log(errorCode);
      this.router.navigate(['/login']);
   });
    //  .then
   //  (res => { console.log('Succesful Signup', res);
   //  this.router.navigate(['/login']);
   // }
   //  )
 }

  

  logout(){
    this.afAuth.auth.signOut().then(res => console.log('Yahuuuu', res));
    this.router.navigate (['/login'])
  }

  login(email:string, password:string){
    this.afAuth
    .auth.signInWithEmailAndPassword(email,password)
    .catch(function(error) {
      // Handle Errors here.
      // var errorCode = error.code;
      var errorMessage = error.message;
      alert(errorMessage);
      console.log(error);
    });
    // .then(
      // res => { console.log('Succesful login', res);
  //  }
    // )
  }

}
