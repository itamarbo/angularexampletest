import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(public authService:AuthService, private router:Router, private route:ActivatedRoute) { }

  email:string;
  password:string;

  ngOnInit() {
  }

  onSubmit(){
    this.authService.login(this.email, this.password);
    this.router.navigate(['/successlogin']);
  }


}
