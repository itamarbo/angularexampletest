import { ImageService } from './../image.service';
import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-classified',
  templateUrl: './classified.component.html',
  styleUrls: ['./classified.component.css']
})
export class ClassifiedComponent implements OnInit {

  category:string = "Loading...";
  categoryImage:string;
  categories:object[] = [{id:1,cat: 'business'}, {id:2,cat: 'entertainment'}, {id:3,cat: 'politics'},{id:4,cat: 'sport'}, {id:5,cat: 'tech'}];
  title:string;
  Category:string;
  id:string;
  userId:string;

  constructor(public classifyService:ClassifyService, public imageService:ImageService, private router:Router, private route:ActivatedRoute,  public authservice:AuthService) { }

  ngOnInit() {
    this.classifyService.classify().subscribe(
      res => {
        this.category = this.classifyService.categories[res];
        this.categoryImage = this.imageService.images[res];
      }
    )
  }

  onSubmit() {
    this.classifyService.addClassify(this.userId,this.title,this.Category);
   this.router.navigate(['/tableclassify']);
    }


}
