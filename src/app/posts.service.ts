import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  myurl="https://jsonplaceholder.typicode.com/posts";

  getPosts(){
    return this.httpClient.get(this.myurl);
  }

  addPosts(title:string, body:string,){
    const post = {title:title, body:body,};
    this.db.collection('posts').add(post);
  }

  constructor(public httpClient:HttpClient, private db:AngularFirestore) { }
}
