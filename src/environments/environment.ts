// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAb5RC8yzPauBJ1VQ74Cg3UqK7OLtrhack",
    authDomain: "angularexampletest.firebaseapp.com",
    databaseURL: "https://angularexampletest.firebaseio.com",
    projectId: "angularexampletest",
    storageBucket: "angularexampletest.appspot.com",
    messagingSenderId: "660910568939",
    appId: "1:660910568939:web:22d3e44b16035260af95ae",
    measurementId: "G-FXBHWV01P9"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
